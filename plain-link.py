#!/usr/bin/env python3
# vim:fileencoding=utf-8

import notify2
import pyperclip
from qutescript import userscript


@userscript
def get_link_as_plain(request):
    title = remove_site_gumph(request.title)
    text = u'{} {}'.format(title, request.url)
    notify2.init('plain links qutebrowser plugin')
    pyperclip.copy(text)
    n = notify2.Notification("Added to clipboard",
                             "'{}' added to clipboard".format(text),
                             'notification-message-im')
    n.show()


def remove_site_gumph(title):
    if 'GitLab' in title:
        return remove_gitlab_gumph(title)

    return title


def remove_gitlab_gumph(title):
    return title.split('·')[0]


if __name__ == '__main__':
    get_link_as_plain()
