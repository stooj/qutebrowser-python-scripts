#!/usr/bin/env python3
# vim:fileencoding=utf-8

import notify2
import pyperclip
from qutescript import userscript


@userscript
def get_link_as_markdown(request):
    text = u'[{}]({})'.format(request.title, request.url)
    notify2.init('Markdown links qutebrowser plugin')
    pyperclip.copy(text)
    n = notify2.Notification("Added to clipboard",
                             "'{}' added to clipboard".format(text),
                             'notification-message-im')
    n.show()


if __name__ == '__main__':
    get_link_as_markdown()
