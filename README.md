# Qutebrowser user scripts

From [GitHub - hiway/python-qutescript: Painless userscripts for qutebrowser.](https://github.com/hiway/python-qutescript)


## Install scripts

To install a script, run the following:

```bash
python path/to/my/script --install --bin=myscript
```

To run the script, execute the following in qutebrowser:

```
:spawn --userscript myscript
```
